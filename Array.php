<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jawaban nomor 3</title>
</head>

<body>
    <h1>Kids and Adults</h1>
    <?php
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    $adults = ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"];

    $totalKids = count($kids);
    $totalAdults = count($adults);

    echo 'Kids :';

    foreach ($kids as $key => $kid) {
        echo '"' . $kid . '"';
        if ($key < ($totalKids - 1)) {
            echo ", ";
        }
    }
    echo '<br>Adults :';

    foreach ($adults as $key => $adult) {
        echo '"' . $adult . '"'; {
            if ($key < ($totalAdults - 1)) {
                echo ", ";
            }
        }
    }

    ?>
</body>

</html>